//package assign.services;
//
//import static org.junit.Assert.*;
//
//import java.util.logging.Logger;
//
//import org.junit.Before;
//import org.junit.Test;
//
//import assign.domain.Course;
//import assign.domain.NewCourse;
//import assign.domain.Project;
//
//public class TestEavesdropServiceImpl {
//	
//	EavesdropService csService = null;
//        Logger testLogger = Logger.getLogger("testlogger");
//	
//	@Before
//	public void setUp() {
//		//"jdbc:mysql://fall-2016.cs.utexas.edu:3306/cs378_projects"
//		String dburl = "jdbc:mysql://fall-2016.cs.utexas.edu:3306/cs378_projects";
//		String dbusername = "chiquini";
//		String dbpassword = "DtU068oexs";
//		csService = new EavesdropServiceImpl(dburl, dbusername, dbpassword);
//	}
//	
//	@Test
//	public void testCourseAddition() {
//		try {
//			NewCourse c = new NewCourse();
//			c.setName("Introduction to Computer Science.");
//			c.setCourseNum("CS102");
//			c = csService.addCourse(c);
//			
//			NewCourse c1 = csService.getCourse(c.getCourseId());
//			
//			assertEquals(c1.getName(), c.getName());
//			assertEquals(c1.getCourseNum(), c.getCourseNum());
//			assertEquals(c1.getCourseId(), c.getCourseId());			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//    @Test
//	public void testCourseGet() {
//	try {
//	    NewCourse c = new NewCourse();
//	    c.setName("Introduction to Computer Science.");
//	    c.setCourseNum("CS102");
//	    c = csService.addCourse(c);
//	    
//	    NewCourse c1 = csService.getCourse_correct(c.getCourseId());
//	    testLogger.info(c1.getName());
//	    testLogger.info(c1.getCourseNum());
//	    testLogger.info(String.valueOf(c1.getCourseId()));
//	    assertEquals(c1.getName(), c.getName());
//	    assertEquals(c1.getCourseNum(), c.getCourseNum());
//	    assertEquals(c1.getCourseId(), c.getCourseId());
//	} catch (Exception e) {
//	    e.printStackTrace();
//	}
//    }
//    
//    @Test
//	public void testProjectAddition() {
//		try {
//			Project p = new Project();
//			p.setName("Introduction to Computer Science.");
//			p.setDescription("CS102");
//			p = csService.addProject(p);
//			
//			Project p1 = csService.getProject(p.getProjectId());
//			
//			assertEquals(p1.getName(), p.getName());
//			assertEquals(p1.getProjectId(), p.getProjectId());			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//    
//    
//    
//    
//    @Test
//   	public void testProjectGet() {
//   	try {
//   	    Project p = new Project();
//   	    p.setName("Batman Suit");
//   	    //p.setProjectId(1);
//   	    p.setDescription("Descrpition");
//   	    p = csService.addProject(p);
//   	    
//   	    Project p1 = csService.getProject(p.getProjectId());
//   	    testLogger.info(p1.getName());
//   	    testLogger.info(String.valueOf(p1.getProjectId()));
//   	    assertEquals(p1.getName(), p.getName());
//   	    assertEquals(p1.getProjectId(), p.getProjectId());
//   	} catch (Exception e) {
//   	    e.printStackTrace();
//   	}
//    }
//}
