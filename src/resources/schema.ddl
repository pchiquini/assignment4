create projectsdb;

use projectsdb;

create table projects(name varchar(255) NOT NULL, projectId int AUTO_INCREMENT, description varchar(255) NOT NULL, PRIMARY KEY(projectId));