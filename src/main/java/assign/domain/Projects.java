package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
//specifies that Projecs objects should be marshalled into XML
//element named <projects>
@XmlRootElement(name = "projects") 
@XmlAccessorType(XmlAccessType.FIELD)
public class Projects {
	    
	String name;
	int projectId;
	
    private List<String> project = null;
    
    public List<String> getProjects() {
    	return project;
    }
    
    public void setProjects(List<String> projects) {
    	this.project = projects;
    }
    
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getProjectId() {
		return projectId;
	}
	
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
}
