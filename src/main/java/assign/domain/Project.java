package assign.domain;

import java.io.InputStream;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
public class Project {

	int projectId;
	String name;
	String description;
		
	List<String> link = null;

	public int getProjectId()
	{
		return this.projectId;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public void setProjectId(int projectId)
	{
		this.projectId = projectId;
	}
	   
	public void setName(String name) { 
		this.name = name;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public List<String> getLink() {
        return link;
    }
 
    public void setLink(List<String> link) {
        this.link = link;
    }
    
    public void deleteProject(int projectId){
    	//this.projectId.
    	this.projectId = projectId;
    }

}
