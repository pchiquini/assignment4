package assign.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

//import com.mysql.fabric.Response;
import javax.ws.rs.core.Response;

import assign.domain.Course;
import assign.domain.Courses;
import assign.domain.Error;
import assign.domain.Project;
import assign.domain.Projects;
import assign.services.EavesdropService;
import assign.services.EavesdropServiceImpl;

@Path("/projects")
public class EavesdropResource {
	
	EavesdropService eavesdropService;
	
	String password;
	String username;
	String dburl;	
	//String database;
	
	private Map<Integer, Project> projectsDB = new ConcurrentHashMap<Integer, Project>(); 
	private AtomicInteger idCounter = new AtomicInteger();
	
//	public EavesdropResource() {
//		this.eavesdropService = new EavesdropService();
//	}
	
	public EavesdropResource(@Context ServletContext servletContext) {
		//"jdbc:mysql://fall-2016.cs.utexas.edu:3306/cs378_projects"
		String dbhost = servletContext.getInitParameter("DBHOST");
		username = servletContext.getInitParameter("DBUSERNAME");
		password = servletContext.getInitParameter("DBPASSWORD");
		String database = servletContext.getInitParameter("DBNAME");
		dburl = "jdbc:mysql://" + dbhost +":3306/"+ database;
		this.eavesdropService = new EavesdropServiceImpl(dburl, username, password);		
	}
			
	@GET
	@Path("/helloworld")
	@Produces("text/html")
	public String helloWorld() {
		System.out.println("Inside helloworld");
		System.out.println("DB creds are:");
		System.out.println("DBURL:" + dburl);
		System.out.println("DBUsername:" + username);
		System.out.println("DBPassword:" + password);		
		return "Hello world " + dburl + " " + username + " " + password;		
	}
	//curl -i --data "<project><projectId></projectId><name>spiderman</name><description>hello</description></project>" -H "Content-Type: application/xml" -X "POST" http://localhost:8080/assignment4/myeavesdrop/projects/		
	@POST
	@Consumes("application/xml")
	public Response createProject(Project project) throws Exception{
		
		project.setDescription("Project representing " + project.getName());
		
		eavesdropService.addProject(project);
		
		System.out.println("Created project " + project.getProjectId());
		return Response.created(URI.create("/projects/"+project.getProjectId())).build();
	}

	@GET
	@Path("/{projectId}")
	@Produces("application/xml")
	public StreamingOutput getProject(@PathParam("projectId") int projectId) throws Exception{
		
		final Project projectResult = eavesdropService.getProject(projectId);
		
		if (projectResult == null){
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return new StreamingOutput() {
	         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	            outputProject(outputStream, projectResult);
	         }
	      };     
	}
	
	//curl -i --data "<project><projectId>5</projectId><name>assign.domain.Project@52a15403</name><description>hello</description></project>" -H "Content-Type: application/xml" -X "PUT" http://localhost:8080/assignment4/myeavesdrop/projects/5	
	@PUT
	@Path("/{projectId}")
	@Consumes("application/xml")
	public void updateProject(@PathParam("projectId") int projectId, Project project) throws Exception {

		Project current = eavesdropService.getProject(projectId);

		if(current == null){
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		else{	
			current.setDescription("HELLO description of " + current.getName());
			eavesdropService.updateProject(projectId,project);
			throw new WebApplicationException(Response.Status.OK);
		}

	}


	//curl -i --data "<project><projectId>8</projectId><name></name><description></description></project>" -H "Content-Type: application/xml" -X "DELETE" http://localhost:8080/assignment4/myeavesdrop/projects/8
	@DELETE
	@Path("/{projectId}")
	public void delete(@PathParam("projectId") int projectId) throws Exception{
		
		Project delete = eavesdropService.getProject(projectId);
		
		if (delete == null){
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		
		else{
			delete = eavesdropService.deleteProject(projectId);
			throw new WebApplicationException(Response.Status.OK);
		}
	}
	
	protected void outputCourses(OutputStream os, Courses course) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Projects.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(course, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}	
	
	protected void outputProjects(OutputStream os, Projects projects) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Projects.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(projects, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}	
	
	protected void outputProject(OutputStream os, Project project) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(project, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
		
	protected void outputError(OutputStream os, Error error) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Error.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(error, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
	protected void outputMessage(OutputStream os, Project project) throws IOException {
		try { 
			JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(project, os);
		} catch (JAXBException jaxb) {
			jaxb.printStackTrace();
			throw new WebApplicationException();
		}
	}
	
//	protected Project readProject(InputStream is) {
//	      try {
//	         DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//	         Document doc = builder.parse(is);
//	         Element root = doc.getDocumentElement();
//	         Project cust = new Project();
//	         if (root.getAttribute("id") != null && !root.getAttribute("id").trim().equals(""))
//	            cust.setProjectId(Integer.valueOf(root.getAttribute("id")));
//	         NodeList nodes = root.getChildNodes();
//	         for (int i = 0; i < nodes.getLength(); i++) {
//	            Element element = (Element) nodes.item(i);
//	            if (element.getTagName().equals("name")) {
//	               cust.setName(element.getTextContent());
//	            }
//	         }
//	         return cust;
//	      }
//	      catch (Exception e) {
//	         throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
//	      }
//	   }
	
}



//@GET
//@Path("/project")
//@Produces("application/xml")
//public StreamingOutput getProject() throws Exception {
//	
//	final Project heat = new Project();
//	heat.setName("%23heat");
//	heat.setLink(new ArrayList<String>());
//	heat.getLink().add("l3");
//	heat.getLink().add("l2");		
//	
//	//throw new WebApplicationException();
//	
//	final NotFound notFound = new NotFound();
//	notFound.setError("Project non-existent-project does not exist");
//	
//    return new StreamingOutput() {
//         public void write(OutputStream outputStream) throws IOException, WebApplicationException {
//            outputCourses(outputStream, notFound);
//         }
//      };
//     
//}		
