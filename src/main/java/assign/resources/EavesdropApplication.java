package assign.resources;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/myeavesdrop")
public class EavesdropApplication extends Application {
	
		//the classes set up is used to hold the names of those classes within your application that should be 
		//instantiated for every incoming request. Since resource classes typically fall into that category, 
		//because it is alright to create a new resource object for handling a request,
		//you should add the names of the resource class to that set
	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> classes = new HashSet<Class<?>>();
	
	public EavesdropApplication() {		
	}
	
	//get a set of root resource classes
	//default life-cycle for resource class instances is 'per-request'
	@Override
	public Set<Class<?>> getClasses() {
		classes.add(EavesdropResource.class);
		return classes;
	}
	
	//get a set of objects that are singletons within our application (shared accross diff requests)
	//singletons.add(myMapp);
	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
	
	
}
