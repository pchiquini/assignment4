package assign.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import assign.domain.Course;
import assign.domain.NewCourse;
import assign.domain.Project;
import assign.domain.Projects;

public class EavesdropServiceImpl implements EavesdropService {

	//"jdbc:mysql://fall-2016.cs.utexas.edu:3306/cs378_projects"; //mysql -h fall-2016.cs.utexas.edu -u chiquini -p cs378_chiquini
		//database server(domain name/ip address : database port (always standard 3306) :databasename
		//jdbc is the protocol
		//:mysql type of database
	String dbURL; 
	String dbUsername; //chiquini
	String dbPassword; //DtU068oexs
	DataSource ds;

	// DB connection information would typically be read from a config file.
	public EavesdropServiceImpl(String dbUrl, String username, String password) {
		this.dbURL = dbUrl;
		this.dbUsername = username;
		this.dbPassword = password;
		
		ds = setupDataSource();
	}
	
	public DataSource setupDataSource() {
        BasicDataSource ds = new BasicDataSource();
        ds.setUsername(this.dbUsername);
        ds.setPassword(this.dbPassword);
        ds.setUrl(this.dbURL);
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        return ds;
    }
	
	public NewCourse addCourse(NewCourse c) throws Exception {
		Connection conn = ds.getConnection();
		
		String insert = "INSERT INTO courses(name, course_num) VALUES(?, ?)";
		PreparedStatement stmt = conn.prepareStatement(insert,
                Statement.RETURN_GENERATED_KEYS);
		
		stmt.setString(1, c.getName());						//setting parameeters  //number represents index "the first ?, from left to right"
		stmt.setString(2, c.getCourseNum());
		
		int affectedRows = stmt.executeUpdate();

        if (affectedRows == 0) {
            throw new SQLException("Creating course failed, no rows affected.");
        }
        
        ResultSet generatedKeys = stmt.getGeneratedKeys(); 	//grabbing the generated key, "wahtever the val on the first column 
        if (generatedKeys.next()) {
        	c.setCourseId(generatedKeys.getInt(1));			//specifying the column of the result set
        }
        else {
            throw new SQLException("Creating course failed, no ID obtained.");
        }
        
        // Close the connection
        conn.close();
        
		return c;
	}
	
	@Override
	public Project addProject(Project project) throws Exception {
		Connection conn = ds.getConnection(); 
		
		String insert = "INSERT INTO projects(name, projectId, description) VALUES(?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(insert,
                Statement.RETURN_GENERATED_KEYS);
		
		stmt.setString(1, project.getName());
		stmt.setLong(2, project.getProjectId());
		stmt.setString(3, project.getDescription());
		
		
		int affectedRows = stmt.executeUpdate(); //creating new row in the table //db generates a new ID

        if (affectedRows == 0) {
            throw new SQLException("Creating project failed, no rows affected.");
        }
        
        ResultSet generatedKeys = stmt.getGeneratedKeys();   //treated as query, as RSobject  //single row single column
        if (generatedKeys.next()) {
        	project.setProjectId(generatedKeys.getInt(1));  //passing the index of the column of result set
        }
        else {
            throw new SQLException("Creating project failed, no ID obtained.");
        }
        
        conn.close();
        
		return project;
		
	}

	public NewCourse getCourse(int courseId) throws Exception {
		String query = "select * from courses where course_id=" + courseId; //concanitation 
		Connection conn = ds.getConnection();								//this comes in from user input NO SANITIZATION
		PreparedStatement s = conn.prepareStatement(query);
		ResultSet r = s.executeQuery();
		
		if (!r.next()) {
			return null;
		}
		
		NewCourse c = new NewCourse();
		c.setCourseNum(r.getString("course_num"));
		c.setName(r.getString("name"));
		c.setCourseId(r.getInt("course_id"));
		return c;
	}

    public NewCourse getCourse_correct(int courseId) throws Exception {
	String query = "select * from courses where course_id=?";   //no string concanitation, we are using a placehodler //SANITIZATION performed
	Connection conn = ds.getConnection();
	PreparedStatement s = conn.prepareStatement(query);  		//query sent to db, creates a plan, anything sent afterwards will be sent as data and NOT exectuable statements
	s.setString(1, String.valueOf(courseId));					//setting value here  
																//this prevent SQL injection 
	ResultSet r = s.executeQuery();  //result set retuned. represents all records in the set
	
//	post: insert  //	get: select  //	delete: delete call
//	put/post/delete: executeUpdate   //	get: use executeQuery

	//if the result can be 0 or 1, then move the pointer to match crietira.
	
	if (!r.next()) { //r.next moves pointer until it returns false
	    return null; //we get the results here
	}
	
	//if it matches then you are pointing to it so you extrace the values
	//you need to know the type of the column and the column names (what it is being passed) ///you can rely on mysql> describe courses 
	NewCourse c = new NewCourse();				
	c.setCourseNum(r.getString("course_num"));
	c.setName(r.getString("name"));
	c.setCourseId(r.getInt("course_id"));
	return c;
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getProjects(Projects projects) throws Exception{
		String query = "SELECT * FROM projects";
		Connection conn = ds.getConnection();
		PreparedStatement s = conn.prepareStatement(query);
		ResultSet r = s.executeQuery();
		s.setString(1, String.valueOf(projects));
		
		List<String> getProjects = new ArrayList<String>();
		
		while (!r.next()) {
		Projects c = new Projects();  
		c.setName(r.getString("name")); 
		c.setProjectId(r.getInt("course_id")); 
		getProjects.addAll((Collection<? extends String>) c);
		}
		
		return getProjects;
	}

	@Override 
	public Project getProject(int projectId) throws Exception {
		String query = "SELECT * FROM projects WHERE projectId=?";  //projectId;
		Connection conn = ds.getConnection();
		PreparedStatement stmt = conn.prepareStatement(query); 
		stmt.setString(1, String.valueOf(projectId));				//stmt.setString(1, String.valueOf(projectId));//putting val in placeholder
		ResultSet r = stmt.executeQuery();    //result set is the entire set
		

		if (!r.next()) {
			return null;
		}

		Project project = new Project();
		project.setName(r.getString("name"));
		project.setProjectId(r.getInt("projectid"));
		project.setDescription("Project representing " + r.getString("name") );
	
		return project;
	}

	@Override
	public Project deleteProject(int projectId) throws Exception {
		String delete = "DELETE FROM projects WHERE projectId=?";// VALUES(?)";
		Connection conn = ds.getConnection();
		PreparedStatement stmt = conn.prepareStatement(delete);// ,Statement.NO_GENERATED_KEYS);
		stmt.setString(1, String.valueOf(projectId));
		int affectedRows = stmt.executeUpdate();
		
        conn.close();
		
		return null;
	}

	@Override
	public Project updateProject(int projectId, Project projecto) throws Exception {
		String name = projecto.getName();//projecto.getName();
		String update = "UPDATE projects SET description='Updated description of " + name + "'  WHERE projectId =?" ;
		//String test = "SELECT * FROM projects WHERE projectId=?";
		Connection conn = ds.getConnection();
		PreparedStatement stmt = conn.prepareStatement(update);
		//stmt.setString(1, String.valueOf(projectId));
		//stmt.setString(1, projecto.getName());
		stmt.setString(1, String.valueOf(projectId));
		
		int r = stmt.executeUpdate(); //r =affectedRows
	
		Project project = new Project();
		project.getProjectId();
		project.getName();
		project.getDescription();
        conn.close();
        return null;
		
	}
}
