package assign.services;

import java.util.List;
import java.util.ListIterator;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import assign.domain.NewCourse;
import assign.domain.Project;
import assign.domain.Projects;


public interface EavesdropService {
	
	public NewCourse addCourse(NewCourse c) throws Exception;
	
	public NewCourse getCourse(int courseId) throws Exception;

    public NewCourse getCourse_correct(int courseId) throws Exception;
    
    public List<String> getProjects(Projects projects) throws Exception;

	public Project getProject(int projectId) throws Exception;
		
	public Project addProject(Project project) throws Exception;
	
	public Project updateProject(int projectId, Project project) throws Exception;
	
	public Project deleteProject(int projectId) throws Exception;

}
